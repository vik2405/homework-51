import React, { Component } from 'react';
function Item() {
    return(
        <div className="items">
            <div className="item1 item">
                <h4>Resources</h4>
                <p>Easily manage your inspiration and work-in-progress by dragging images into projects and sharable client groups. Easily manage your inspiration and work-in-progress</p><a href="#">Explore more →</a>
            </div>
            <div className="item2 item">
                <h4>Training & Funding</h4>
                <p>Easily manage your inspiration and work-in-progress by dragging images into projects and sharable client groups.</p><a href="#">Browse training →</a>
            </div>
            <div className="item3 item">
                <h4>Connect</h4>
                <p>Easily manage your inspiration and work-in-progress by dragging images into projects and sharable client groups. Easily manage your inspiration.</p><a href="#">Connect now →</a>
            </div>
        </div>
    )
}
export default Item;