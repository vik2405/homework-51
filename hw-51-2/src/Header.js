import React, { Component } from 'react';
function Header() {
    return (
        <div className="header">
            <div className="container">
                <a href="#" className="logo"><img src="img/logo.png" alt="" width="151" height="47"/></a>
                <div className="navigator">
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">How It Works</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}
            export default Header;