import React, { Component } from 'react';
function Inform() {
    return(
        <div className="inform">
            <div className="info">
                <div className="text">
                    <h3><span>it’s all about </span>fitness first</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud.</p>
                    <a href="#">Read More</a>
                </div>
                <div className="bigphoto"><img src="img/photo1.jpeg" alt=""/></div>
            </div>

            <div className="info">
                <div className="bigphoto"><img src="img/photo2.jpeg" alt=""/></div>
                <div className="text">
                    <h3><span>love your</span>your body</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud.</p>
                    <a href="#">Read More</a>
                </div>
            </div>
        </div>
    )
}
export default Inform;