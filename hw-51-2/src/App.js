import React, {Component} from 'react';
import './App.css';
import Header from "./Header"
import Item from "./Item"
import Inform from "./Inform"
import Footer from "./Footer"
class App extends Component {
    render() {
        return (
            <div className="App">
                <Header/>
                <div className="container">
                    <Item/>
                    <Inform/>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default App;
